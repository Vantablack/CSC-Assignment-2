﻿var defaultWidget;

var onloadCallback = function () {
    console.info("grecaptcha loaded");
    defaultWidget = grecaptcha.render(document.getElementById('g-recaptcha'), {
        sitekey: '6LeVEiYTAAAAAJV3cf5B-i65iT8V2EkNrGrV7nVv',
        'callback': function (response) {
            if (response) {
                $('#registerButton').prop('disabled', false);
            } else {
                $('#registerButton').prop('disabled', true);
            }
        }
    });
};

$(document).ready(displayToken());

$('#registerButton').click(function (event) {
    register({
        'Email': $('#registerForm').find('[name="Email"]').val(),
        'Password': $('#registerForm').find('[name="Password"]').val(),
        "ConfirmPassword": $('#registerForm').find('[name="ConfirmPassword"]').val(),
        "CaptchaResponse": grecaptcha.getResponse(defaultWidget)
    });
    grecaptcha.reset(defaultWidget);
    $('#registerButton').prop('disabled', true);
});

$('#loginButton').click(function () {
    login({
        'username': $('#loginForm').find('[name="Email"]').val(),
        'password': $('#loginForm').find('[name="Password"]').val(),
        'grant_type': 'password'
    });
});

$('#updateButton').click(function () {
    changePassword({
        'OldPassword': $('#updateForm').find('[name="OldPassword"]').val(),
        'NewPassword': $('#updateForm').find('[name="NewPassword"]').val(),
        "ConfirmPassword": $('#updateForm').find('[name="ConfirmPassword"]').val()
    });
});

$('#logoutButton').click(function () {
    logout();
});

function register(formValue) {
    showAlert('info', 'Making AJAX request');
    $.ajax({
        type: 'POST',
        url: 'api/Account/Register',
        contentType: 'application/json',
        data: JSON.stringify(formValue)
    }).done(function (data, textStatus, jqXHR) {
        showAlert("success", "You have been successfully registered. You may now login.");
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
        console.error('Error in registering:', jqXHR);
    });
}

function login(formValue) {
    showAlert('info', 'Making AJAX request');
    $.ajax({
        type: 'POST',
        url: 'Token',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: formValue
    }).done(function (data, textStatus, jqXHR) {
        showAlert('success', 'Successfully logged in');
        sessionStorage.setItem('accessToken', data.access_token);
        sessionStorage.setItem('userName', data.userName);
        displayToken();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
        console.error('Error with login:', jqXHR);
        displayToken();
    });
}

function changePassword(formValue) {
    showAlert('info', 'Making AJAX request');
    var token = sessionStorage.getItem('accessToken');
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    $.ajax({
        type: 'POST',
        url: 'api/Account/ChangePassword',
        contentType: 'application/json',
        headers: headers,
        data: JSON.stringify(formValue)
    }).done(function (data, textStatus, jqXHR) {
        showAlert('success', 'Successfully updated password');
        sessionStorage.clear();
        displayToken();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
        console.error('Error in updating:', jqXHR);
    });
}

function logout() {
    sessionStorage.clear();
    displayToken();
}

function displayToken() {
    if (sessionStorage.getItem('accessToken')) {
        showAlert('success', 'You are already logged in');
        var string = '<p>You are logged in as <b>' + sessionStorage.getItem('userName') + '</b><br/>Your access token is <code>' + sessionStorage.getItem('accessToken') + '</code></p>';
        $('#loginMessage').html(string).parent().removeClass('hidden-xs-up');
        $('#updateButton').prop('disabled', false);
        $('#logoutButton').prop('disabled', false);
        $('#loginButton').prop('disabled', true);
        getUserInfo();
    } else {
        $('#loginMessage').parent().addClass('hidden-xs-up');
        $('#invokeSubmit').prop('disabled', true);
        $('#updateButton').prop('disabled', true);
        $('#logoutButton').prop('disabled', true);
        $('#loginButton').prop('disabled', false);
        $('#userInfo').html(string).parent().addClass('hidden-xs-up');
    }
}

function getUserInfo() {
    showAlert('info', 'Making AJAX request');
    var token = sessionStorage.getItem('accessToken');
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    $.ajax({
        type: 'GET',
        url: 'api/Account/UserInfo',
        headers: headers
    }).done(function (data, textStatus, jqXHR) {
        var string = '<p>Email: <code>' + data.Email + '</code></p>' + '<p>HasRegistered: <code>' + data.HasRegistered + '</code></p>' + '<p>LoginProvider: <code>' + data.LoginProvider + '</code></p>';
        $('#userInfo').html(string).parent().removeClass('hidden-xs-up');
        $('#updateForm').find("[name='Email']").val(data.Email);
        clearAlert();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
        console.error('Error with login:', jqXHR);
    });
}

function showAlert(type, message) {
    $('#alertPlaceholder').html(`<div class="alert alert-${type} alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>${message}</span></div>`);
}

function clearAlert() {
    $("#alertPlaceholder").empty();
}