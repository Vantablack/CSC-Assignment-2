﻿$(document).ready(getAllPhotos());

function getAllPhotos() {
    showAlert('info', 'Making AJAX request');
    $.ajax({
        type: 'GET',
        url: 'api/Photos'
    }).done(function (data, textStatus, jqXHR) {
        clearAlert();
        showImages(data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
    });
}

function showAlert(type, message) {
    $('#alertPlaceholder').html(`<div class="alert alert-${type} alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>${message}</span></div>`);
}

function clearAlert() {
    $("#alertPlaceholder").empty();
}

function showImages(photoArray) {
    var gallery = $('#gallery');
    photoArray.forEach(function (currentItem, index, array) {
        var template = `<div class="col-xs-12 col-sm-6 col-md-4" id="${currentItem.PhotoId}"><div class="card" style="overflow: hidden;"><img class="card-img-top" src="${currentItem.PhotoUrl}" alt="${currentItem.PhotoBlobName}" style="display: block; max-height: 180px; margin: 0 auto;"><div class="card-block"><a href="${currentItem.PhotoUrl}" target="_blank" class="btn btn-primary btn-block">Download</a></div></div></div>`;
        console.log(template);
        $(gallery).append($(template));
    });

}