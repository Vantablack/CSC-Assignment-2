﻿$(document).ready(checkLoginStatus());

$('#uploadPhoto').on('click', function () {
    uploadPhotos();
});

$('#photo').change(function () {
    if ($(this).val()) {
        $('#uploadPhoto').prop('disabled', false);
    } else {
        $('#uploadPhoto').prop('disabled', true);
    }
});

function checkLoginStatus() {
    if (!sessionStorage.getItem('accessToken')) {
        showAlert('danger', '<p>You are not logged in</p><a href="/Profile" class="btn btn-primary">Go to Login</a>');
    } else {
        getAllPhotos();
        $('#updateButton').prop('disabled', false);
    }
}

function showAlert(type, message) {
    $('#alertPlaceholder').html(`<div class="alert alert-${type} alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>${message}</span></div>`);
}

function clearAlert() {
    $("#alertPlaceholder").empty();
}

function showImages(photoArray) {
    var $gallery = $('#gallery');
    $gallery.empty();
    photoArray.forEach(function (currentItem, index, array) {
        var template = `<div class="col-xs-12 col-sm-6 col-md-4" id="${currentItem.PhotoId}"><div class="card" style="overflow: hidden;"><img class="card-img-top" src="${currentItem.PhotoUrl}" alt="${currentItem.PhotoBlobName}" style="display: block; max-height: 180px; margin: 0 auto;"><div class="card-block"><button type="button" class ="btn btn-danger btn-block" onclick="deletePhoto('${currentItem.PhotoId}')">Delete</button><a href="${currentItem.PhotoUrl}" target="_blank" class="btn btn-primary btn-block">Download</a></div></div></div>`;
        $gallery.append($(template));
    });
}

function getAllPhotos() {
    showAlert('info', 'Making AJAX request');
    var token = sessionStorage.getItem('accessToken');
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    $.ajax({
        type: 'GET',
        url: 'api/Photos',
        headers: headers
    }).done(function (data, textStatus, jqXHR) {
        clearAlert();
        showImages(data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
    });
}

function uploadPhotos() {
    showAlert('info', 'Making AJAX request');
    var token = sessionStorage.getItem('accessToken');
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    var formData = new FormData();
    $.each($('#photo')[0].files, function (i, file) {
        formData.append('file-' + i, file);
    });

    $.ajax({
        type: 'POST',
        url: 'api/Photos',
        headers: headers,
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data, textStatus, jqXHR) {
        clearAlert();
        getAllPhotos();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
    });
}

function deletePhoto(id) {
    showAlert('info', 'Making AJAX request');
    var token = sessionStorage.getItem('accessToken');
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    $.ajax({
        type: 'DELETE',
        url: `api/Photos/${id}`,
        headers: headers
    }).done(function (data, textStatus, jqXHR) {
        clearAlert();
        $(`#${id}`).remove();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
    });
}