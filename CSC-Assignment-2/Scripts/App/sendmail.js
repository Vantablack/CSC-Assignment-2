﻿var defaultWidget;

var onloadCallback = function () {
    console.info("grecaptcha loaded");
    defaultWidget = grecaptcha.render(document.getElementById('g-recaptcha'), {
        sitekey: '6LeVEiYTAAAAAJV3cf5B-i65iT8V2EkNrGrV7nVv',
        'callback': function (response) {
            if (response) {
                $('#submitButton').prop('disabled', false);
            } else {
                $('#submitButton').prop('disabled', true);
            }
        }
    });
};

$('#submitButton').click(function (event) {
    mail({
        'ToEmail': $('#mailForm').find('[name="Email"]').val(),
        'Message': $('#mailForm').find('[name="Message"]').val(),
        "CaptchaResponse": grecaptcha.getResponse(defaultWidget)
    });
    grecaptcha.reset(defaultWidget);
    $('#submitButton').prop('disabled', true);
});

function mail(formValue) {
    var soapMessage = `<?xml version="1.0" encoding="utf-8"?> <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <soap:Body> <SendMail xmlns="http://tempuri.org/"> <ToEmail>${formValue.ToEmail}</ToEmail> <Message>${formValue.Message}</Message> <CaptchaResponse>${formValue.CaptchaResponse}</CaptchaResponse> </SendMail> </soap:Body> </soap:Envelope>`;
    showAlert('info', 'Making AJAX request');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/SOAP/Mail.asmx',
        contentType: "text/xml; charset=\"utf-8\"",
        dataType: "xml",
        data: soapMessage,
        processData: false
    }).done(function (data, textStatus, jqXHR) {
        if (!!$(data).find("SendMailResult").text()) {
            showAlert("success", `Sent mail to <b>${formValue.ToEmail}</b> successfully`);
        } else {
            showAlert("danger", `Fail to send mail to <b>${formValue.ToEmail}</b>`);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showAlert('danger', '<p>Error:</p><code>' + JSON.stringify(jqXHR, null, 2) + '</code>');
    });
}

function showAlert(type, message) {
    $('#alertPlaceholder').html(`<div class="alert alert-${type} alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>${message}</span></div>`);
}

function clearAlert() {
    $("#alertPlaceholder").empty();
}
