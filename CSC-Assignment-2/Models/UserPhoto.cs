﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSC_Assignment_2.Models
{
    public class UserPhoto
    {
        public string PhotoId { get; set; }
        public string UserId { get; set; }
        public string PhotoBlobName { get; set; }
        public string PhotoUrl { get; set; }

    } // end of class UserPhoto
}