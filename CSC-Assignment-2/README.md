﻿References:

- [_Layout.cshtml](http://www.tutorialsteacher.com/mvc/layout-view-in-asp.net-mvc)
- [@Html.ActionLink()](https://msdn.microsoft.com/en-us/library/system.web.mvc.html.linkextensions.actionlink(v=vs.118).aspx#M:System.Web.Mvc.Html.LinkExtensions.ActionLink%28System.Web.Mvc.HtmlHelper,System.String,System.String,System.Object,System.Object%29)
- [Individual Accounts](http://www.asp.net/web-api/overview/security/individual-accounts-in-web-api)