﻿using CSC_Assignment_2.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Services;

namespace CSC_Assignment_2.SOAP
{
    /// <summary>
    /// Summary description for Mail
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Mail : System.Web.Services.WebService
    {

        [WebMethod]
        [Authorize]
        public bool SendMail(string ToEmail, string Message, string CaptchaResponse)
        {
            if (!AccountController.VerifyCaptcha(CaptchaResponse))
            {
                return false;
            }
            var body = Message;
            var message = new MailMessage();
            message.To.Add(new MailAddress(ToEmail));
            message.Subject = "CSC Assignment 2 Email Service";
            message.Body = "User <b>" + User.Identity.Name + "</b> has sent you an email<br/>" + Message;
            message.IsBodyHtml = true;
            using (var smtp = new SmtpClient())
            {
                smtp.Send(message);
                return true;
            }
        } // end of SendMail()
    } // end of class Mail
}
