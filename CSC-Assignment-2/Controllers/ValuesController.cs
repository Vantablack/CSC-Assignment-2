﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace CSC_Assignment_2.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public object Get()
        {
            //return User.Identity.Name;
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            string userId = principal.Identity.GetUserId();

            var user = new
            {
                id = principal.Identity.GetUserId(),
                name = principal.Identity.GetUserName()
            };

            return user;
        }

        public string Get(string CountryName, string CityName)
        {
            using (var client = new WebClient())
            {
                var response = client.DownloadString($"http://www.webservicex.net/globalweather.asmx/GetWeather?CityName={CityName}&CountryName={CountryName}");
                return response;
            }
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container.
            CloudBlobContainer container = blobClient.GetContainerReference("csccontainer");

            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();

            // Set permission of blob storage
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference("testBlob");

            // Create or overwrite the "myblob" blob with contents from a local file.
            blockBlob.UploadText(value);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
