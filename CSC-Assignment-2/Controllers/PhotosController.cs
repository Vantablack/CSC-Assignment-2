﻿using System;
using System.Web.Http;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using System.IO;
using System.Web;
using Microsoft.AspNet.Identity;
using CSC_Assignment_2.Models;
using CSC_Assignment_2.Database;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CSC_Assignment_2.Controllers
{
    public class PhotosController : ApiController
    {
        public string ToEmail { get; private set; }

        // GET: api/Photos
        public List<UserPhoto> Get()
        {
            UserPhotoDb photoDb = new UserPhotoDb();
            string id = User.Identity.GetUserId();
            return string.IsNullOrEmpty(id) ? photoDb.GetAllPhoto() : photoDb.GetAllPhotoByUserId(id);
        } // end of Get()

        // GET: api/Photos/5
        public string Get(int id)
        {
            return "value";
        }

        [Authorize]
        // POST: api/Photos
        public async Task<HttpResponseMessage> Post()
        {
            var httpRequest = HttpContext.Current.Request;
            string userId = User.Identity.GetUserId();

            for (int index = 0; index < httpRequest.Files.Count; index++)
            {
                HttpPostedFile postedFile = httpRequest.Files[index];
                // Check image mime type
                if (postedFile.ContentType.ToLower() != "image/jpg" &&
                    postedFile.ContentType.ToLower() != "image/jpeg" &&
                    postedFile.ContentType.ToLower() != "image/pjpeg" &&
                    postedFile.ContentType.ToLower() != "image/gif" &&
                    postedFile.ContentType.ToLower() != "image/x-png" &&
                    postedFile.ContentType.ToLower() != "image/png")
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
                    && Path.GetExtension(postedFile.FileName).ToLower() != ".png"
                    && Path.GetExtension(postedFile.FileName).ToLower() != ".gif"
                    && Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                // Reference: http://stackoverflow.com/questions/359894/how-to-create-byte-array-from-httppostedfile
                // Converting posted file into a byte array
                using (var binaryReader = new BinaryReader(postedFile.InputStream))
                {
                    // Parse the connection string and return a reference to the storage account.
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                        CloudConfigurationManager.GetSetting("StorageConnectionString"));

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve a reference to a container.
                    CloudBlobContainer container = blobClient.GetContainerReference("photos");

                    // Create the container if it doesn't already exist.
                    container.CreateIfNotExists();

                    // Set permission of blob storage
                    container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                    // Generating an unique name for the new photo
                    string extension = System.IO.Path.GetExtension(httpRequest.Files[index].FileName);
                    string photoId = Guid.NewGuid().ToString();

                    // Retrieve reference to a blob
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(photoId + extension);

                    // Create or overwrite the  blob with contents from a local file.
                    await blockBlob.UploadFromByteArrayAsync(binaryReader.ReadBytes(postedFile.ContentLength), 0, postedFile.ContentLength);

                    UserPhoto photo = new UserPhoto();
                    photo.UserId = userId;
                    photo.PhotoId = photoId;
                    photo.PhotoBlobName = photoId + extension;
                    UserPhotoDb photoDb = new UserPhotoDb();
                    photoDb.AddPhoto(photo);

                    string body = "Thank you for uploading an image. Your image is available <a href='{0}'>here</a>.";
                    body = string.Format(body, ConfigurationManager.AppSettings["AzurePhotoContainerUrl"].ToString() + photo.PhotoBlobName);
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(User.Identity.Name));
                    message.Subject = "CSC Assignment 2 Email Service";
                    message.Body = body;
                    message.IsBodyHtml = true;
                    using (var smtp = new SmtpClient())
                    {
                        await smtp.SendMailAsync(message);
                        return Request.CreateResponse(HttpStatusCode.Created);
                    }
                }
            } // end of for()
            return Request.CreateResponse(HttpStatusCode.NoContent);
        } // end of Post()

        // PUT: api/Photos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize]
        // DELETE: api/Photos/5
        public HttpResponseMessage Delete(string id)
        {
            UserPhoto userPhoto = new UserPhoto();
            userPhoto.UserId = User.Identity.GetUserId();
            userPhoto.PhotoId = id;
            UserPhotoDb photoDb = new UserPhotoDb();
            try
            {
                if (photoDb.DeleteOnePhotoById(userPhoto))
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                throw new Exception("Unknown error");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
