﻿using CSC_Assignment_2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types

namespace CSC_Assignment_2.Database
{
    public class UserPhotoDb
    {
        public void AddPhoto(UserPhoto inPhoto)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "INSERT INTO UserPhoto (PhotoId, UserId, PhotoBlobName) VALUES (@inPhotoId, @inUserId, @inBlobName);";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inPhotoId", SqlDbType.VarChar, 128).Value = inPhoto.PhotoId;
                    cmd.Parameters.Add("@inUserId", SqlDbType.VarChar, 128).Value = inPhoto.UserId;
                    cmd.Parameters.Add("@inBlobName", SqlDbType.VarChar, 200).Value = inPhoto.PhotoBlobName;
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of AddPhoto()

        public List<UserPhoto> GetAllPhoto()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            List<UserPhoto> photoList = new List<UserPhoto>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT * FROM UserPhoto;";
                    cmd.CommandText = sqlText;
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds, "PhotoData");
                        foreach (DataRow dataRow in ds.Tables["PhotoData"].Rows)
                        {
                            UserPhoto photo = new UserPhoto();
                            photo.PhotoId = dataRow["PhotoId"].ToString();
                            photo.PhotoBlobName = dataRow["PhotoBlobName"].ToString();
                            photo.PhotoUrl = ConfigurationManager.AppSettings["AzurePhotoContainerUrl"] + dataRow["PhotoBlobName"].ToString();
                            photoList.Add(photo);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return photoList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllPhotos()

        public List<UserPhoto> GetAllPhotoByUserId(string inUserId)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            List<UserPhoto> photoList = new List<UserPhoto>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT * FROM UserPhoto WHERE UserId = @inUserId;";
                    cmd.Parameters.Add("@inUserId", SqlDbType.VarChar, 128).Value = inUserId;
                    cmd.CommandText = sqlText;
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds, "PhotoData");
                        foreach (DataRow dataRow in ds.Tables["PhotoData"].Rows)
                        {
                            UserPhoto photo = new UserPhoto();
                            photo.PhotoId = dataRow["PhotoId"].ToString();
                            photo.PhotoBlobName = dataRow["PhotoBlobName"].ToString();
                            photo.PhotoUrl = ConfigurationManager.AppSettings["AzurePhotoContainerUrl"] + dataRow["PhotoBlobName"].ToString();
                            photoList.Add(photo);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return photoList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllPhotoByUserId()

        public bool DeleteOnePhotoById(UserPhoto inPhoto)
        {
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    // Start a local transaction.
                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;

                    string selectSql = "SELECT PhotoBlobName FROM UserPhoto WHERE PhotoId = @inPhotoId;";
                    string deleteSql = "DELETE FROM UserPhoto WHERE PhotoId = @inPhotoId AND UserId = @inUserId;";

                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        cmd.CommandText = selectSql;
                        cmd.Parameters.Add("@inPhotoId", SqlDbType.NVarChar).Value = inPhoto.PhotoId;
                        da.SelectCommand = cmd;
                        da.Fill(ds, "PhotoData");
                        // Getting the blobName from database
                        string blobName = ds.Tables["PhotoData"].Rows[0]["PhotoBlobName"].ToString();

                        if (blobName == null)
                        {
                            throw new Exception("No photo found.");
                        }

                        // Retrieve storage account from connection string.
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                            CloudConfigurationManager.GetSetting("StorageConnectionString"));

                        // Create the blob client.
                        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                        // Retrieve reference to a previously created container.
                        CloudBlobContainer container = blobClient.GetContainerReference("photos");

                        // Retrieve reference to a blob named "myblob.txt".
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);

                        // Delete the blob.
                        blockBlob.Delete();

                        // Clear and prepare delete
                        cmd.Parameters.Clear();
                        cmd.CommandText = deleteSql;
                        cmd.Parameters.Add("@inPhotoId", SqlDbType.NVarChar).Value = inPhoto.PhotoId;
                        cmd.Parameters.Add("@inUserId", SqlDbType.NVarChar).Value = inPhoto.UserId;
                        int rowsAffected = cmd.ExecuteNonQuery();
                        status = (rowsAffected > 0);
                        //No errors, commit the database
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of DeleteOnePhotoById()
    } // end of class UserPhotoDb
}