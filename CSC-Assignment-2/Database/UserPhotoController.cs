﻿using CSC_Assignment_2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CSC_Assignment_2.Database
{
    public class UserPhotoController
    {
        public bool AddUserPhoto(UserPhoto userPhoto)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "INSERT INTO Attribute (AttributeName) VALUES (@inAttributeName);";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inAttributeName", SqlDbType.VarChar, 50).Value = inAttribute.AttributeName;
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("UC_AttributeName"))
                        {
                            throw new Exception("Attribute name exists in database.");
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of AddUserPhoto()
    } // end of class UserPhotoController
}