﻿using System.Web;
using System.Web.Optimization;

namespace CSC_Assignment_2
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/tether/tether.min.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/app/profile").Include(
                      "~/Scripts/App/profile.js"));

            bundles.Add(new ScriptBundle("~/bundles/app/index").Include(
                      "~/Scripts/App/index.js"));

            bundles.Add(new ScriptBundle("~/bundles/app/userphotos").Include(
                      "~/Scripts/App/userphotos.js"));

            bundles.Add(new ScriptBundle("~/bundles/app/sendmail").Include(
                      "~/Scripts/App/sendmail.js"));
        }
    }
}
